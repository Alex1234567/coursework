﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HealthProject.MobileModels
{
    public class ChangeUnitParameter
    {
        public string TargetUnitParameterId { get; set; }

        public string UserParameterId { get; set; }
    }
}
