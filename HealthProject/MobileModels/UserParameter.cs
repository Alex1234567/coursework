﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HealthProject.MobileModels
{
    public class UserParameter
    {
        public string Id { get; set; }
        public string ParameterName { get; set; }
        public string UnitName { get; set; }

        public string ParameterNameUnitName
        {
            get
            {
                return String.Format("{0}, {1}", ParameterName, UnitName);
            }
        }
    }
}
