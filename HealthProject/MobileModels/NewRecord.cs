﻿using System;

namespace HealthProject.MobileModels
{
    public class NewRecord
    {
        public string Id { get; set; }

        public string UserParameterId { get; set; }

        public DateTime Date { get; set; }

        public double Value { get; set; }
    }
}
