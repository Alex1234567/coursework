﻿using HealthProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HealthProject.MobileModels
{
    public class ParametersAndUnits
    {
        public List<string> ParametersNames { get; set; }

        public List<Unit> Units { get; set; }
    }
}
