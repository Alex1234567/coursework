﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HealthProject.MobileModels
{
    public class Record
    {
        public string Id { get; set; }

        public DateTime Date { get; set; }

        public double Value { get; set; }
    }
}
