﻿using System;

namespace HealthProject.MobileModels
{
    public class UnitParameter
    {
        public string Id { get; set; }
        public string ParameterName { get; set; }
        public string UnitName { get; set; }
        public string UserParameterId { get; set; }

        public string Name
        {
            get
            {
                return ParameterName;
            }
        }

        public string Action
        {
            get
            {
                return UserParameterId == null ? "Добавить" : "Удалить";
            }
        }
    }
}
