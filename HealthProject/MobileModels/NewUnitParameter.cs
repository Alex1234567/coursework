﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HealthProject.MobileModels
{
    public class NewUnitParameter
    {
        public string ParameterName { get; set; }
        public string UnitId { get; set; }
        public string UnitName { get; set; }
    }
}
