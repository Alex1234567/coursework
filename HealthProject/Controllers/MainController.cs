﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using HealthProject.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System;
using Microsoft.Extensions.Configuration;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authorization;
using System.Threading.Tasks;
using HealthProject.ViewModels;
using System.Net;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace HealthProject.Controllers
{
    public class MainController : Controller
    {
        private readonly HealthContext health_db;
        string webInterfaceSystemId;
        string pathToExportImages;
        public MainController(HealthContext health_db, IConfiguration configuration)
        {
            this.health_db = health_db;

            var identificatorsConfig = configuration.GetSection("Identificators");
            this.webInterfaceSystemId = identificatorsConfig.GetValue<string>("WebInterfaceSystemId");

            var pathsConfig = configuration.GetSection("Paths");
            this.pathToExportImages = pathsConfig.GetValue<string>("PathToExportPlots");
        }

        [HttpGet]
         public IActionResult GetPlot(string tokenValue, string strBegin, string strEnd, string parameterName)      
        {
            DateTime begin = DateTime.Parse(strBegin);
            DateTime end = DateTime.Parse(strEnd);
            var token = health_db.Tokens.Find(tokenValue);

            if (token == null)
                return Content("Нет пользователя с таким токеном в системе");
            var userId = token.UserId;
            var systemId = token.SystemId;

            var userParameter = health_db.UsersParameters
                    .Where(up => up.UnitParameter.Parameter.Name.ToLower() == parameterName.ToLower()
                            && up.UserId == userId
                            && up.SubscribeDate != null)
                    .Include(up => up.UnitParameter.Parameter)
                    .Include(up => up.UnitParameter.Unit)
                    .FirstOrDefault();


            if (userParameter == null)
                return Content("Пользователь не подписан на запрошенный показатель или такого показателя нет в системе");



            var records = from r in health_db.Records
                          where r.UserParameterId == userParameter.Id &&
                          r.Date.CompareTo(begin) >= 0 &&
                          r.Date.CompareTo(end) <= 0
                          orderby r.Date
                          select r;
           

            var plt = new ScottPlot.Plot(600, 600);
            // turn off features which typically shorten tick label size
            plt.Ticks(useOffsetNotation: false, useMultiplierNotation: false);

            // tightening with a render is the best way to get the axes right
            plt.TightenLayout(render: true);
            //plt.PlotSignal(price, sampleRate: pointsPerDay, xOffset: start.ToOADate());
            if(records.Count() > 0)
            {
                double[] dataY;
                double[] dataX;

                dataY = new double[records.Count()];
                dataX = new double[records.Count()];

                int i = 0;
                foreach (var r in records)
                {
                    dataX[i] = r.Date.ToOADate();
                    dataY[i] = r.Value;
                    i++;
                }
                plt.PlotScatter(dataX, dataY);
            }
            
            plt.Ticks(dateTimeX: true);
            plt.YLabel("Значение");
            plt.XLabel("Дата");
            plt.Title(userParameter.UnitParameter.ParameterNameUnitName);

            var myUniqueFileName = string.Format(@"{0}.png", Guid.NewGuid());
            var pathToImage = Path.Combine(pathToExportImages, myUniqueFileName);

            plt.SaveFig(pathToImage);

            var image = System.IO.File.OpenRead(Path.GetFullPath(pathToImage));
            return File(image, "image/png");           
        }
        [HttpPost]
        public IActionResult AddRecordToUser(string tokenValue, string parameterName, string strDate, string strValue)
        {
            double value = 0;
            double.TryParse(strValue, out value);
            DateTime date = DateTime.Now;
            DateTime.TryParse(strDate, out date);

            var token = health_db.Tokens.Find(tokenValue);

            if (token == null)
                return Content("Нет пользователя с таким токеном в системе");
            var userId = token.UserId;
            var systemId = token.SystemId;

            var userParameter = health_db.UsersParameters
                    .Where(up => up.UnitParameter.Parameter.Name.ToLower() == parameterName.ToLower()
                            && up.UserId == userId
                            && up.SubscribeDate != null)
                    .Include(up => up.UnitParameter.Parameter)
                    .Include(up => up.UnitParameter.Unit)
                    .FirstOrDefault();

            if (userParameter == null)
                return Content("Пользователь не подписан на запрошенный показатель или такого показателя нет в системе");

            health_db.Records.Add(new Record
            {
                SystemId = systemId,
                UserParameterId = userParameter.Id,
                Date = date,
                Value = value

            });
            health_db.SaveChanges();

            return Content("Успешно добавлено");
        }

        [HttpGet]
        [Authorize]
        public ActionResult AddUserParameter()
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            HealthContext db = health_db;

            //берем все показатели
            var allParameters = db.Parameters;

            //берем показатели, которые уже есть у пользователя        
            var userParameters = from up in db.UsersParameters
                                      where up.SubscribeDate.HasValue && up.UserId == userId
                                      select up.UnitParameter.Parameter;

            //получаем показатели, которых нет у пользователя 
            var notUserParameters = allParameters.Except(userParameters);

            //получаем соответствующие показатели с ед. измерения
            var notUserUnitsParameters = from up in db.UnitsParameters
                                             join p in notUserParameters on up.ParameterId equals p.Id
                                             select up;

            //подгружаем для каждого показателя с ед. измерения сам показатель и ед. измерения через внешний ключ
            var result = 
                notUserUnitsParameters
                .OrderBy(up => up.Parameter.Name)
                .ThenBy(up => up.Unit.Name)
                    .Include(up => up.Parameter)
                    .Include(up => up.Unit);

            return View(result);
        }

        [HttpPost]
        [Authorize]
        public ActionResult AddUserParameter(string unitParameterId)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            health_db.AddUserParameter(userId, unitParameterId, true);

            return RedirectToAction("Main", "Main");
        }

        [HttpGet]
        [Authorize]
        public ActionResult DeleteUserParameter(string userParameterId)
        {
            UserParameter userParameter = health_db.UsersParameters
                .Where(up => up.Id == userParameterId)
                .Include(up => up.UnitParameter)
                    .ThenInclude(up => up.Parameter)
                .Include(up => up.UnitParameter)
                    .ThenInclude(up => up.Unit)
                .FirstOrDefault();
            
            return View(userParameter);
        }

        [HttpPost, ActionName("DeleteUserParameter")]
        [Authorize]
        public ActionResult DeleteUserParameterConfirmed(string userParameterId)
        {
            health_db.UnsubscribeUserParameter(userParameterId);

            return RedirectToAction("Main", "Main");
        }

        [HttpGet]
        [Authorize]
        public IActionResult Main()
        {
            HealthContext db = health_db;

            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            //берем все показатели с ед. измерения на которые подписан пользователь
            var usersParameters = subscribeUserParameters(userId);

            //подгружаем для каждого показателя с ед. измерения у пользователя сам показатель и ед. измерения через внешний ключ
            var result = usersParameters
                .Include(up => up.UnitParameter)
                    .ThenInclude(up => up.Parameter)
                .Include(up => up.UnitParameter)
                    .ThenInclude(up => up.Unit).ToList();

            List<UserParameterModel> mainModel = new List<UserParameterModel>();

            foreach (var up in result)
            {
                var upRecords = from r in db.Records
                              where r.UserParameterId == up.Id
                              orderby r.Date
                              select new SimpleRecordModel { Date = r.Date, Value = r.Value};

                mainModel.Add(new UserParameterModel { UserParameter = up, Records = upRecords.ToList() });
            }

            return View(mainModel);
        }

        [Authorize]
        public ActionResult Records(string userParameterId)//, DateTime datetimeBegin, DateTime datetimeEnd)
        {
            HealthContext db = health_db;

            var userParameter = db.UsersParameters
              .Include(up => up.UnitParameter)
                .ThenInclude(up => up.Parameter)
            .Include(up => up.UnitParameter)
                .ThenInclude(up => up.Unit)
            .SingleOrDefault(up => up.Id == userParameterId);


            var records = from record in db.Records
                          where record.UserParameterId == userParameterId 
                          orderby record.Date
                          select record;

            var result =
                records.Include(r => r.UserParameter)
                        .ThenInclude(up => up.UnitParameter)
                        .ThenInclude(up => up.Parameter)
                    .Include(r => r.UserParameter)
                        .ThenInclude(up => up.UnitParameter)
                        .ThenInclude(up => up.Unit)
                    .Include(r => r.PositionInSource)
                        .ThenInclude(p => p.Source)
                        .ThenInclude(s => s.System)
                    .Include(r => r.System)
                    .ToList();


            return View(new RecordsModel 
            { 
                Records = result, 
                UserParameter =  userParameter
            });
        }
        
        public IOrderedQueryable<UserParameter> subscribeUserParameters(string userId)
        {
            return from up in health_db.UsersParameters
                                  where up.SubscribeDate.HasValue &&
                                  up.UserId == userId
                                  orderby up.UnitParameter.Parameter.Name,
                                          up.UnitParameter.Unit.Name
                                  select up;
        }

        [HttpGet]
        [Authorize]
        public ActionResult CRDUsersParameters()
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            return View(health_db.ListForManageUserParameters(userId));
        }

        [HttpGet]
        [Authorize]
        public ActionResult ChangeSubscribeUserParameter(string unitParameterId)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            var userParameter = health_db.CheckSubscribe(userId, unitParameterId);

            //если подписки нет, значит добавляем
            if (userParameter == null)
                health_db.AddUserParameter(userId, unitParameterId, true);
            else
                health_db.UnsubscribeUserParameter(userParameter.Id);

            return RedirectToAction("CRDUsersParameters", "Main");
        }

        

        [HttpGet]
        [Authorize]
        public ActionResult DeleteRecord(string recordId)
        {
            HealthContext db = health_db;

            //проверяем, что удаляемая запись принадлежит тому пользователю, кто удаляет
            //var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            //var ownerUserId = from record in db.Records
            //                  where record.UserParameter.UserId == userId &&
            //                        record.Id == recordId
            //                  select record;

            var record = health_db.Records
               .Where(r => r.Id == recordId)
                    .Include(r => r.UserParameter)
                        .ThenInclude(up => up.UnitParameter)
                        .ThenInclude(up => up.Parameter)
                    .Include(r => r.UserParameter)
                        .ThenInclude(up => up.UnitParameter)
                        .ThenInclude(up => up.Unit)
                    .Include(r => r.System)
                    .Include(r => r.PositionInSource)
                        .ThenInclude(ps => ps.Source)
                        .ThenInclude(s => s.System)
               .FirstOrDefault();

            return View(record);
        }

        [HttpPost, ActionName("DeleteRecord")]
        [Authorize]
        public ActionResult DeleteRecordConfirmed(string recordId, string userParameterId)
        {
            var record = health_db.Records.Find(recordId);

            health_db.Remove(record);

            health_db.SaveChanges();

            return RedirectToAction("Records", "Main", new { userParameterId });
        }

        [HttpGet]
        [Authorize]
        public ActionResult EditRecord(string recordId)
        {
            HealthContext db = health_db;

            //проверяем, что удаляемая запись принадлежит тому пользователю, кто удаляет
            //var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            //var ownerUserId = from record in db.Records
            //                  where record.UserParameter.UserId == userId &&
            //                        record.Id == recordId
            //                  select record;

            var record = health_db.Records
               .Where(r => r.Id == recordId)
                    .Include(r => r.UserParameter)
                        .ThenInclude(up => up.UnitParameter)
                        .ThenInclude(up => up.Parameter)
                    .Include(r => r.UserParameter)
                        .ThenInclude(up => up.UnitParameter)
                        .ThenInclude(up => up.Unit)
                    .Include(r=> r.PositionInSource)
                        .ThenInclude(ps => ps.Source)
                        .ThenInclude(s => s.System)
                    .Include(r => r.System)
               .FirstOrDefault();

            return View(record);
        }

        [HttpPost]
        [Authorize]
        public ActionResult EditRecord(string recordId, double newValue, DateTime newDate)
        {
            HealthContext db = health_db;

            var record = health_db.Records.Find(recordId);

            record.Value = newValue;
            record.Date = newDate;

            db.SaveChanges();

            return RedirectToAction("Records", "Main", new { userParameterId = record.UserParameterId });
        }

        [HttpGet]
        [Authorize]
        public ActionResult AddRecord(string userParameterId)
        {
            HealthContext db = health_db;

            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            //берем все показатели с ед. измерения на которые подписан пользователь
            //var usersParaneters = from up in db.UsersParameters
            //                      where up.SubscribeDate.HasValue &&
            //                      up.UserId == userId
            //                      orderby up.UnitParameter.Parameter.Name,
            //                              up.UnitParameter.Unit.Name
            //                      select up;

            var userParameter = db.UsersParameters
              .Include(up => up.UnitParameter)
                .ThenInclude(up => up.Parameter)
            .Include(up => up.UnitParameter)
                .ThenInclude(up => up.Unit)
            .SingleOrDefault(up => up.Id == userParameterId);


            //подгружаем для каждого показателя с ед. измерения у пользователя сам показатель и ед. измерения через внешний ключ
            //var result = usersParaneters
            //    .Include(up => up.UnitParameter)
            //        .ThenInclude(up => up.Parameter)
            //    .Include(up => up.UnitParameter)
            //        .ThenInclude(up => up.Unit);

            return View(userParameter);
        }

        [HttpPost]
        [Authorize]
        public ActionResult AddRecord(Record record)//string userParameterId, double value, DateTime date)
        {
            HealthContext db = health_db;

            record.SystemId = webInterfaceSystemId;

            db.Records.Add(record);

            db.SaveChanges();

            return RedirectToAction("Records", "Main", new { userParameterId = record.UserParameterId});
        }

        [HttpGet]
        [Authorize]
        public ActionResult AddParameter()
        {
            HealthContext db = health_db;

            return View(db.Parameters.OrderBy(parameter => parameter.Name));
        }

        [HttpPost]
        [Authorize]
        public ActionResult AddParameter(string nameNewParameter)
        {
            if(!health_db.Parameters.Any(p => p.Name == nameNewParameter))
            {
                health_db.Parameters.Add(new Parameter
                {
                    Name = nameNewParameter.Trim()
                });

                health_db.SaveChanges();
            }

            return RedirectToAction("AddUserParameter", "Main");
        }

        [HttpGet]
        [Authorize]
        public ActionResult AddUnit()
        {
            return View(health_db.Units.OrderBy(unit => unit.Name));
        }

        [HttpPost]
        [Authorize]
        public ActionResult AddUnit(string nameNewUnit)
        {
            if (!health_db.Units.Any(u => u.Name == nameNewUnit))
            {
                health_db.Units.Add(new Unit
                {
                    Name = nameNewUnit.Trim()
                });

                health_db.SaveChanges();
            }

            return RedirectToAction("AddUserParameter", "Main");
        }

        [HttpGet]
        [Authorize]
        public ActionResult AddUnitParameter()
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            
            return View(health_db.AvailableUnitsForUser(userId));
        }

        [HttpGet]
        public ActionResult Notification(string title, string text)
        {
            return View(new Notification { Title = title, Text = text });
        }

        [HttpPost]
        [Authorize]
        public ActionResult AddUnitParameter(string parameterName, string unitName, string unitId, string systemUnits)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            parameterName = parameterName.Trim();
            unitName = unitName == null ? null : unitName.Trim();

            string errors = "";
            if (!health_db.IsUniqParameterNameForUser(userId, parameterName))
                errors = "Похожий показатель уже существует.\n";
            //если пользователь выбрал не встроенные единицы измерения
            if (systemUnits != "on" && !health_db.IsUniqUnitNameForUser(userId, unitName))
                errors += "Похожие ед. изм. уже существуют.";

            if (errors != "")
                return RedirectToAction("Notification", "Main", new { title = "Ошибка", text = errors });
            
            health_db.AddUnitParameter(userId, parameterName,
               systemUnits=="on" ? unitId : null, unitName);

            return RedirectToAction("Main", "Main");
        }
    }
    }