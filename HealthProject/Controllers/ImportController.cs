﻿using HealthProject.Classes.SerializeJSON;
using HealthProject.Models;
using MetadataExtractor;
using MetadataExtractor.Formats.Exif;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text.Json;
using Directory = MetadataExtractor.Directory;

namespace HealthProject.Controllers
{
    public class ImportController : Controller
    {
        private readonly HealthContext health_db;
        string pathToPythonScript;
        string pathToPythonInterpreter;
        string pathToImportFiles;
        string pathToImportImages;
        string pathToConfirmRecognizedValuesView;

        public ImportController(HealthContext health_db, IConfiguration configuration)
        {
            this.health_db = health_db;

            var pathsConfig = configuration.GetSection("Paths");
            this.pathToPythonScript = pathsConfig.GetValue<string>("PathToPythonScript");
            this.pathToPythonInterpreter = pathsConfig.GetValue<string>("PathToPythonInterpreter");
            this.pathToImportFiles = pathsConfig.GetValue<string>("PathToImportFiles");
            this.pathToImportImages = pathsConfig.GetValue<string>("PathToImportImages");
            this.pathToConfirmRecognizedValuesView = pathsConfig.GetValue<string>("PathToConfirmRecognizedValuesView");
            

            var identificatorsConfig = configuration.GetSection("Identificators");

            
        }

        [HttpGet]
        [Authorize]
        public ActionResult Import()
        {
            return View();
        }

        void deleteOldRecords(string userId, string systemId)
        {
            var recordsForDelete = from r in health_db.Records
                                   where r.UserParameter.UserId == userId &&
                                   r.PositionInSource.Source.SystemId == systemId
                                   select r;
            health_db.Records.RemoveRange(recordsForDelete);
            health_db.SaveChanges();
        }

        [HttpPost]
        public void RecordsJSON(string tokenValue, [FromBody]object body)//,string tokenValue
        {
            //check if token exist
            var token = health_db.Tokens.Find(tokenValue);
            if (token == null)// || uploadedFile == null)
                return;

            var userId = token.UserId;
            var systemId = token.SystemId;

            deleteOldRecords(userId, systemId);

            var myUniqueFileName = string.Format(@"{0}.json", Guid.NewGuid());
            var path = Path.Combine(pathToImportFiles, myUniqueFileName);

            //save file
            var jsonString = body.ToString();
            using (StreamWriter file = new StreamWriter(path))
                file.WriteLine(jsonString);

            List<RecordsUnitParameter> records = new List<RecordsUnitParameter>();

            //var jsonString = System.IO.File.ReadAllText(uploadedFile.FileName);

            records = JsonSerializer.Deserialize<List<RecordsUnitParameter>>(jsonString);

            var newFile = new Models.File { Path = path };
            health_db.Files.Add(newFile);

            var newSource = new Source { FileId = newFile.Id, SystemId = systemId };
            health_db.Sources.Add(newSource);

            foreach (var recordsUnitParameter in records)
            {
                //check existing unitParameter
                var unitParameterId = recordsUnitParameter.UnitParameterId;
                if (health_db.UnitsParameters.Find(unitParameterId) != null)
                {
                    var userParameter = health_db.AddUserParameterIfNotExist(userId, unitParameterId, false);                   

                    //add records in database
                    int position = 0;
                    PositionInSource positionInSource;
                    foreach (var dv in recordsUnitParameter.DatesValues)
                    {
                        positionInSource = new PositionInSource
                        {
                            Position = position.ToString(),
                            SourceId = newSource.Id
                        };

                        health_db.PositionsInSources.Add(positionInSource);

                        health_db.Records.Add(new Record
                        {
                            UserParameterId = userParameter.Id,
                            PositionInSourceId = positionInSource.Id,
                            Date = dv.Date,
                            Value = dv.Value
                        });

                        position++;
                    }

                    health_db.SaveChanges();
                }
            }

            return;
        }

        [HttpPost]
        [Authorize]
        public ActionResult GoogleFit(IFormFile uploadedFile)
        {
            if (uploadedFile == null)
                return RedirectToAction("GoogleFit", "Import");

            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            //if (uploadedFile == null)
            //    return;

            //var userId = token.UserId;
            //var systemId = token.SystemId;

            var myUniqueFileName = string.Format(@"{0}.csv", Guid.NewGuid());
            var path = Path.Combine(pathToImportFiles, myUniqueFileName);
            //string path = uploadedFile.FileName;
            // save file
            using (var fileStream = new FileStream(path, FileMode.Create))
            {
                uploadedFile.CopyTo(fileStream);
            }

            var userParameter = health_db.AddUserParameterIfNotExist(userId, HealthContext.stepsUnitParameterId, false);           

            var newFile = new Models.File { Path = path };
            health_db.Files.Add(newFile);

            var newSource = new Source { FileId = newFile.Id, SystemId = HealthContext.googleFitSystemId };
            health_db.Sources.Add(newSource);

            using (StreamReader reader = new StreamReader(path))
            {
                string row;
                int position = 0;
                PositionInSource positionInSource;

                DateTime date;
                int steps;

                while ((row = reader.ReadLine()) != null)
                {
                    var datas = row.Split(',');

                    if (DateTime.TryParse(datas[0], out date) &&
                        int.TryParse(datas[6], out steps))
                    {
                        positionInSource = new PositionInSource
                        {
                            Position = position.ToString(),
                            SourceId = newSource.Id
                        };

                        health_db.PositionsInSources.Add(positionInSource);

                        health_db.Records.Add(new Record
                        {
                            UserParameterId = userParameter.Id,
                            PositionInSourceId = positionInSource.Id,
                            Date = date,
                            Value = steps
                        });
                    }

                    position++;
                }
            }

            health_db.SaveChanges();

            return RedirectToAction("Main","Main");

        }

        [HttpGet]
        [Authorize]
        public ActionResult LoadPhotoTonometer()
        {
            return View();
        }

        [HttpPost]
        [Authorize]
        public ActionResult LoadPhotoTonometer(IFormFile uploadedFile)//, DateTime dateModification)
        {
            if (uploadedFile == null)
                return RedirectToAction("LoadPhotoTonometer", "Import");

            TonometerRecord tr = 
                Classes.Import.ImportMethods.RecognizeTonometerPhoto
                (pathToImportImages, uploadedFile, pathToPythonScript, pathToPythonInterpreter, false);

            return View(pathToConfirmRecognizedValuesView, tr);
        }

        [HttpPost]
        [Authorize]
        public ActionResult ConfirmRecognizedValues(TonometerRecord tonometerRecord)//, DateTime dateModification)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            health_db.ConfirmRecognizedValues(userId, tonometerRecord);

            return RedirectToAction("Main", "Main");
        }

        [HttpGet]
        [Authorize]
        public ActionResult Tokens()
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            HealthContext db = health_db;

            var tokens = from token in db.Tokens.Include(t => t.System)
                         where token.UserId == userId
                         select token;

            return View(tokens);
        }

        [HttpGet]
        [Authorize]
        public ActionResult AddToken()
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            HealthContext db = health_db;

            var freeSystems = from system in db.Systems
                              where !db.Tokens.Any(token => token.SystemId == system.Id && token.UserId == userId)
                              orderby system.Name
                              select system;

            return View(freeSystems);
        }

        [HttpPost]
        [Authorize]
        public ActionResult AddToken(string systemId)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            HealthContext db = health_db;

            var system = db.Systems.Find(systemId);

            if(system != null)
            {
                db.Tokens.Add(new Token { SystemId = systemId, UserId = userId });
                db.SaveChanges();
            }

            return RedirectToAction("Tokens","Import");
        }

        [HttpGet]
        [Authorize]
        public ActionResult DeleteToken(string tokenValue)
        {
            var token = health_db.Tokens.Find(tokenValue);

            if (token != null)
            {
                var system = health_db.Systems.Find(token.SystemId);
                token.System = system;
                return View(token);
            }
            else
                return RedirectToAction("Tokens", "Import");
        }

        [HttpPost, ActionName("DeleteToken")]
        [Authorize]
        public ActionResult DeleteTokenConfirmed(string tokenValue)
        {
            var token = health_db.Tokens.Find(tokenValue);

            health_db.Tokens.Remove(token);

            health_db.SaveChanges();

            return RedirectToAction("Tokens", "Import");
        }
    }
}