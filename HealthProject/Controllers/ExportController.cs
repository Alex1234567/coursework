﻿using HealthProject.Classes.Export;
using HealthProject.Classes.ExportData;
using HealthProject.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;


namespace HealthProject.Controllers
{
    public class ExportController : Controller
    {
        private readonly HealthContext health_db;

        string pathToExportFiles;
        string pathToExportReports;
        string pathToMessageEventExportReportView;
        public ExportController(HealthContext health_db, IConfiguration configuration)
        {
            this.health_db = health_db;

            var pathsConfig = configuration.GetSection("Paths");
            this.pathToExportFiles = pathsConfig.GetValue<string>("PathToExportFiles");
            this.pathToExportReports = pathsConfig.GetValue<string>("PathToExportReports");
            this.pathToMessageEventExportReportView = pathsConfig.GetValue<string>("PathToMessageEventExportReportView");

            var emailConfig = configuration.GetSection("EmailConfig");
            SenderEmails.hostMailServer = emailConfig.GetValue<string>("HostMailServer");
            SenderEmails.portMailServer = emailConfig.GetValue<int>("PortMailServer");
            SenderEmails.name = emailConfig.GetValue<string>("Name");
            SenderEmails.address = emailConfig.GetValue<string>("Address");
            SenderEmails.password = emailConfig.GetValue<string>("Password");    
        }

        [HttpGet]
        [Authorize]
        public ActionResult Export()
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            //берем все показатели с ед. измерения на которые подписан пользователь
            var usersParameters = from up in health_db.UsersParameters
                                  where up.SubscribeDate.HasValue &&
                                  up.UserId == userId
                                  orderby up.UnitParameter.Parameter.Name,
                                          up.UnitParameter.Unit.Name
                                  select up;

            //подгружаем для каждого показателя с ед. измерения у пользователя сам показатель и ед. измерения через внешний ключ
            var result = usersParameters
                .Include(up => up.UnitParameter)
                    .ThenInclude(up => up.Parameter)
                .Include(up => up.UnitParameter)
                    .ThenInclude(up => up.Unit);

            return View(result);
        }

        [HttpPost]
        [Authorize]
        public ActionResult Export(List<string> userParametersId, string emails)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            var userRecords = from r in health_db.Records.Include(r => r.UserParameter)
                                  .ThenInclude(up => up.UnitParameter)
                                  .ThenInclude(up => up.Parameter)
                              where r.UserParameter.UserId == userId
                              orderby r.UserParameter.UnitParameter.Parameter.Name, r.Date
                              select r;

            var result = userRecords
                .Include(r => r.UserParameter)
                    .ThenInclude(up => up.UnitParameter)
                    .ThenInclude(up => up.Parameter)
                .Include(r => r.UserParameter)
                    .ThenInclude(up => up.UnitParameter)
                    .ThenInclude(up => up.Unit);

            ExportData.pathToExportFiles = pathToExportFiles;
            ExportData.CsvToEmails(result.ToList(), emails);

            return View("MessageEventExport");
        }


        [HttpGet]
        [Authorize]
        public ActionResult ExportReport()
        {
            return View(health_db.Reports);
        }

        [HttpPost]
        [Authorize]
        public ActionResult ExportReport(DateTime datetimeBegin, DateTime datetimeEnd, string reportId, string emails)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            HealthContext db = health_db;

            var reportParameters = from reportParameter in db.ReportsParameters
                                   where reportParameter.ReportId == reportId
                                   select reportParameter;

            reportParameters = reportParameters
                .Include(rp => rp.Report)
                .Include(rp => rp.TypeView);

            List<ReportParameterWithRecords> reportParameterWithRecords = new List<ReportParameterWithRecords>();

            foreach (var reportParameter in reportParameters)
            {
                var records = from record in db.Records
                              .Include(r => r.UserParameter)
                                .ThenInclude(up => up.UnitParameter)
                                .ThenInclude(up => up.Parameter)
                             .Include(r => r.UserParameter)
                                .ThenInclude(up => up.UnitParameter)
                                .ThenInclude(up => up.Unit)
                              where record.UserParameter.User.Id == userId &&
                                  record.UserParameter.UnitParameter.Parameter.Id == reportParameter.ParameterId
                              orderby record.Date
                              select record;

                reportParameterWithRecords.Add(new ReportParameterWithRecords { ReportParameter = reportParameter, Records = records });
            }
            Classes.Export.ExportReport.pathToExportReports = pathToExportReports;
            Classes.Export.ExportReport.ReportToEmails(reportParameterWithRecords, datetimeBegin, datetimeEnd, emails);

            return View(pathToMessageEventExportReportView);
        }

    }
}