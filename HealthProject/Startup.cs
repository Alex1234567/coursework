using HealthProject.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace HealthProject
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        void InitIdentificators()
        {
            var identificatorsConfig = Configuration.GetSection("Identificators");

            HealthContext.tonometerSystemId = identificatorsConfig.GetValue<string>("TonometerSystemId");
            HealthContext.googleFitSystemId = identificatorsConfig.GetValue<string>("GoogleFitSystemId");

            HealthContext.pulseUnitParameterId = identificatorsConfig.GetValue<string>("PulseUnitParameterId");
            HealthContext.topPressUnitParameterId = identificatorsConfig.GetValue<string>("TopPressUnitParameterId");
            HealthContext.lowerPressUnitParameterId = identificatorsConfig.GetValue<string>("LowerPressUnitParameterId");
            HealthContext.stepsUnitParameterId = identificatorsConfig.GetValue<string>("StepsUnitParameterId");
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //        services.AddDbContextPool<HealthContext>(options =>
            //options.UseNpgsql(Configuration.GetConnectionString("DefaultConnection")));
            InitIdentificators();

            services.AddDbContext<HealthContext>(options =>
    options.UseNpgsql(Configuration.GetConnectionString("DefaultConnection")));

            services.AddIdentity<User, IdentityRole>()
    .AddEntityFrameworkStores<HealthContext>();

            services.AddSingleton<IConfiguration>(Configuration);

            services.AddControllersWithViews();           
            
            var jwtConfig = Configuration.GetSection("JwtConstants");
            var issuer = jwtConfig.GetValue<string>("Issuer");
            var audience = jwtConfig.GetValue<string>("Audience");
            var key = jwtConfig.GetValue<string>("Key");

            services.AddAuthentication()
                .AddCookie(cfg => cfg.SlidingExpiration = true)
                .AddJwtBearer(cfg =>
                    {
                        cfg.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters()
                        {
                            ValidIssuer = issuer,
                            ValidAudience = audience,
                            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key)),
                            ValidateLifetime = false
                        };
                        
                    }
                );

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Main}/{action=Main}/{id?}");
            });
        }
    }
}
