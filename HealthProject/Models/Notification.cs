﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HealthProject.Models
{
    public class Notification
    {
        public string Title { get; set; }

        public string Text { get; set; }
    }
}
