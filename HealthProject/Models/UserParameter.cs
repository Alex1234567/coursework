﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace HealthProject.Models
{
    public class UserParameter
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public string Id { get; set; }

        public string UserId { get; set; }

        public User User { get; set; }

        public string UnitParameterId { get; set; }

        public UnitParameter UnitParameter { get; set; }

        public DateTime? SubscribeDate { get; set; }

        public DateTime? UnsubscribeDate { get; set; }

        public bool MainUserParameter { get; set; }
    }
}
