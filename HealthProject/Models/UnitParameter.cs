﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace HealthProject.Models
{
    public class UnitParameter
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public string Id { get; set; }

        public string ParameterId { get; set; }

        public Parameter Parameter { get; set; }

        public string UnitId { get; set; }

        public Unit Unit { get; set; }

        public bool IsBase { get; set; }

        public string AuthorUserId { get; set; }

        public string ParameterNameUnitName { 
            get { 
                return Parameter.Name + ", " + Unit.Name; 
            } 
        }
    }
}
