﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace HealthProject.Models
{
    public class Token
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public string Value { get; set; }

        public string UserId { get; set; }

        public User User { get; set; }

        public string SystemId { get; set; }

        public System System { get; set; }
    }
}
