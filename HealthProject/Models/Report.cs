﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HealthProject.Models
{
    public class Report
    {
        public string Id { get; set; }

        public string Name { get; set; }
    }
}
