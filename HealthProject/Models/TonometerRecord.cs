﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HealthProject.Models
{
    public class TonometerRecord
    {
        public double TopPressure { get; set; }
        public double LowerPressure { get; set; }
        public double Pulse { get; set; }
        public DateTime Date { get; set; }
        public string PathFile { get; set; }
        public bool IsPulse { get; set; }
    }
}
