﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace HealthProject.Models
{
    public class Record
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public string Id { get; set; }

        public string UserParameterId { get; set; }

        public UserParameter UserParameter { get; set; }

        public string PositionInSourceId { get; set; }

        public PositionInSource PositionInSource { get; set; }

        public string SystemId { get; set; }
        public System System { get; set; }

        public double Value { get; set; }

        public DateTime Date { get; set; }

        public string SystemName { 
            get 
            {
                string str = PositionInSourceId == null ? System.Name
                    : PositionInSource.Source.System.Name;
                return
                    str;
            } }
}
}
