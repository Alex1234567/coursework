﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;




namespace HealthProject.Models
{
    public class HealthContext : IdentityDbContext<User>
    {
        public static string tonometerSystemId { get; set; }
        public static string pulseUnitParameterId { get; set; }
        public static string topPressUnitParameterId { get; set; }
        public static string lowerPressUnitParameterId { get; set; }
        public static string stepsUnitParameterId { get; set; }
        public static string googleFitSystemId { get; set; }


        public DbSet<Parameter> Parameters { get; set; }
        public DbSet<Unit> Units { get; set; }
        public DbSet<UnitParameter> UnitsParameters { get; set; }
        public DbSet<UserParameter> UsersParameters { get; set; }
        public DbSet<Record> Records { get; set; }
        public DbSet<Token> Tokens { get; set; }
        public DbSet<PositionInSource> PositionsInSources { get; set; }
        public DbSet<Source> Sources { get; set; }
        public DbSet<System> Systems { get; set; }
        public DbSet<File> Files { get; set; }
        public DbSet<Report> Reports { get; set; }
        public DbSet<TypeView> TypesView { get; set; }
        public DbSet<ReportParameter> ReportsParameters { get; set; }
        public DbSet<TranslatorUnits> TranslatorsUnits { get; set; }


        public HealthContext(DbContextOptions<HealthContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }

        public UserParameter AddUserParameterIfNotExist(string userId, string unitParameterId, bool inMainMenu)
        {
            //check existing userParameter
            var userParameter = (from up in UsersParameters
                                 where up.UnitParameterId == unitParameterId &&
                                 up.UserId == userId
                                 select up).FirstOrDefault();

            //add userParameter if it doesn't exist
            if (userParameter == null)
            {
                userParameter = new UserParameter
                {
                    UserId = userId,
                    UnitParameterId = unitParameterId,
                    SubscribeDate = DateTime.UtcNow,
                    UnsubscribeDate = null,
                    MainUserParameter = inMainMenu
                };
                UsersParameters.Add(userParameter);
            }

            if (userParameter.SubscribeDate == null)
            {
                userParameter.SubscribeDate = DateTime.UtcNow;
                userParameter.UnsubscribeDate = null;
            }

            SaveChanges();

            return userParameter;
        }

        public string AddUserParameter(string userId, string unitParameterId, bool mainUserParameter)
        {
            //проверяем существует ли уже связь между показателем и пользователем
            var userParameter = (from up in UsersParameters
                                 where up.UserId == userId &&
                                       up.UnitParameterId == unitParameterId
                                 select up).FirstOrDefault();

            //если подписка уже когда-то была, а сейчас ее нет, то обновляем ее
            if (userParameter != null)
            {
                userParameter.SubscribeDate = DateTime.UtcNow;
                userParameter.UnsubscribeDate = null;
            }
            else
            {
                userParameter = new HealthProject.Models.UserParameter
                {
                    UnitParameterId = unitParameterId,
                    UserId = userId,
                    SubscribeDate = DateTime.UtcNow,
                    UnsubscribeDate = null,
                    MainUserParameter = mainUserParameter
                };

                UsersParameters.Add(userParameter);
            }

            SaveChanges();

            return userParameter.Id;
        }

        public void UnsubscribeUserParameter(string userParameterId)
        {
            var userParameter = UsersParameters.Find(userParameterId);

            userParameter.UnsubscribeDate = DateTime.UtcNow;
            userParameter.SubscribeDate = null;

            SaveChanges();
        }

        public void AddRecordWithPosition(int position, string sourceId, string userParameterId, DateTime date, double value)
        {
            PositionInSource positionInSource;

            positionInSource = new PositionInSource
            {
                Position = position.ToString(),
                SourceId = sourceId
            };
            PositionsInSources.Add(positionInSource);

            Records.Add(new Record
            {
                UserParameterId = userParameterId,
                PositionInSourceId = positionInSource.Id,
                Date = date,
                Value = value
            });

            SaveChanges();
        }

        public void ConfirmRecognizedValues(string userId, TonometerRecord tonometerRecord)
        {
            var topPressUserParameter = AddUserParameterIfNotExist(userId, topPressUnitParameterId, false);
            var lowerPressUserParameter = AddUserParameterIfNotExist(userId, lowerPressUnitParameterId, false);                   

            var newFile = new Models.File { Path = tonometerRecord.PathFile };
            Files.Add(newFile);

            var newSource = new Source { FileId = newFile.Id, SystemId = tonometerSystemId };
            Sources.Add(newSource);

            int position = 0;
            AddRecordWithPosition(position, newSource.Id, topPressUserParameter.Id, tonometerRecord.Date, tonometerRecord.TopPressure);
            position++;
            AddRecordWithPosition(position, newSource.Id, lowerPressUserParameter.Id, tonometerRecord.Date, tonometerRecord.LowerPressure);
            
            if (tonometerRecord.IsPulse)
            {
                position++;
                var pulseUserParameter = AddUserParameterIfNotExist(userId, pulseUnitParameterId, false);
                AddRecordWithPosition(position, newSource.Id, pulseUserParameter.Id, tonometerRecord.Date, tonometerRecord.Pulse);
            }
        }

        public List<MobileModels.UserParameter> UserParameters(string userId, bool mainUserParameters)
        {
            //берем все показатели с ед. измерения на которые подписан пользователь
            var usersParameters = from up in UsersParameters
                                  .Include(up => up.UnitParameter)
                                    .ThenInclude(up => up.Parameter)
                                  .Include(up => up.UnitParameter)
                                    .ThenInclude(up => up.Unit)
                                  where up.SubscribeDate.HasValue &&
                                  up.UserId == userId &&
                                  up.MainUserParameter == mainUserParameters
                                  orderby up.UnitParameter.Parameter.Name,
                                          up.UnitParameter.Unit.Name
                                  select new HealthProject.MobileModels.UserParameter
                                  {
                                      Id = up.Id,
                                      ParameterName = up.UnitParameter.Parameter.Name,
                                      UnitName = up.UnitParameter.Unit.Name
                                  };
            return usersParameters.ToList();
        }

        public void ChangeUserParameterSection(string userParameterId)
        {
            var userParameter = UsersParameters.Find(userParameterId);

            userParameter.MainUserParameter = !userParameter.MainUserParameter;

            SaveChanges();
        }

        public bool IsUniqParameterNameForUser(string userId, string parameterName)
        {
            parameterName = parameterName.ToLower();

            var unitParameter = from up in UnitsParameters
                         where (up.AuthorUserId == null ||
                         up.AuthorUserId == userId) &&
                         up.Parameter.Name.ToLower() == parameterName
                         select up;
            return unitParameter.Count() == 0 ? true : false;
        }

        public bool IsUniqUnitNameForUser(string userId, string unitName)
        {
            unitName = unitName.ToLower();

            var unitParameter = from up in UnitsParameters
                                where (up.AuthorUserId == null ||
                                up.AuthorUserId == userId) &&
                                up.Unit.Name.ToLower() == unitName
                                select up;
            return unitParameter.Count() == 0 ? true : false;
        }

        public List<MobileModels.UnitParameter> ListForManageUserParameters(string userId)
        {
            //получаем показатели с ед. измерения на которые подписан пользователь
            var userUnitsParameters = from up in UsersParameters
                                      .Include(up => up.UnitParameter)
                                        .ThenInclude(up => up.Unit)
                                      .Include(up => up.UnitParameter)
                                        .ThenInclude(up => up.Parameter)
                                      where up.SubscribeDate.HasValue && up.UserId == userId
                                      orderby up.UnitParameter.Parameter.Name,
                                             up.UnitParameter.Unit.Name
                                      select new MobileModels.UnitParameter
                                      {
                                          Id = up.UnitParameterId,
                                          ParameterName = up.UnitParameter.Parameter.Name,
                                          UnitName = up.UnitParameter.Unit.Name,
                                          UserParameterId = up.Id
                                      };

            //берем все показатели
            var allParameters = Parameters;

            //берем показатели, которые уже есть у пользователя        
            var userParameters = from up in UsersParameters
                                 where up.SubscribeDate.HasValue && up.UserId == userId
                                 select up.UnitParameter.Parameter;

            //получаем показатели, которых нет у пользователя 
            var notUserParameters = allParameters.Except(userParameters);

            //получаем соответствующие показатели с ед. измерения
            var notUserUnitsParameters = from up in UnitsParameters.Include(up => up.Parameter).Include(up => up.Unit)
                                         join p in notUserParameters on up.ParameterId equals p.Id
                                         where up.IsBase &&
                                            (up.AuthorUserId == null || //берем показатели системные
                                            up.AuthorUserId == userId)//берем показатели которые принадежат текущему пользователю, но он отписан
                                         orderby up.Parameter.Name,
                                                up.Unit.Name
                                         select new MobileModels.UnitParameter
                                         {
                                             Id = up.Id,
                                             ParameterName = up.Parameter.Name,
                                             UnitName = up.Unit.Name,
                                             UserParameterId = null
                                         };

            var listUserUnitsParameters = userUnitsParameters.ToList();
            listUserUnitsParameters.AddRange(notUserUnitsParameters.ToList());

            return listUserUnitsParameters;
        }

        public UserParameter CheckSubscribe(string userId, string unitParameterId)
        {
            return UsersParameters.FirstOrDefault(up => up.UserId == userId &&
            up.UnitParameterId == unitParameterId &&
            up.SubscribeDate != null);
        }

        public List<Unit> AvailableUnitsForUser(string userId)
        {
            return (from up in UnitsParameters
                         where (up.AuthorUserId == null ||
                         up.AuthorUserId == userId)
                         orderby up.Unit.Name
                         select up.Unit).Distinct().ToList();
        }

        public void AddUnitParameter(string userId, string parameterName, string unitId, string unitName)
        {
            var parameter = new Parameter { Name = parameterName };
            Parameters.Add(parameter);

            Unit unit;

            //если нет id ед.изм. значит ед. изм. новые
            if (unitId == null)
            {
                unit = new Unit { Name = unitName };
                Units.Add(unit);
            }
            else
                unit = Units.Find(unitId);

            var unitParameter = new HealthProject.Models.UnitParameter
            {
                UnitId = unit.Id,
                ParameterId = parameter.Id,
                IsBase = true, //пользовательский показатель по умолчанию базовый
                AuthorUserId = userId
            };

            UnitsParameters.Add(unitParameter);
            SaveChanges();


            //сразу подписываем на добавленный показатель
            var userParameter = new HealthProject.Models.UserParameter
            {
                UnitParameterId = unitParameter.Id,
                UserId = userId,
                SubscribeDate = DateTime.UtcNow,
                UnsubscribeDate = null,
                MainUserParameter = true
            };

            UsersParameters.Add(userParameter);

            SaveChanges();
        }
    }
}
