﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HealthProject.Models
{
    public class TranslatorUnits
    {       
        [Key]
        public string Id { get; set; }

        public string UnitId { get; set; }

        public string BaseUnitId { get; set; }

        public double Coefficient { get; set; }
    }
}
