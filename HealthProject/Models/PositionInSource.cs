﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace HealthProject.Models
{
    public class PositionInSource
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public string Id { get; set; }

        public string SourceId { get; set; }

        public Source Source { get; set; }

        [Column(TypeName = "json")]
        public string Position { get; set; }
    }
}
