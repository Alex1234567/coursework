﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json;

namespace HealthProject.Models
{
    public class ReportParameter
    {
        public string Id { get; set; }

        public string ReportId { get; set; }

        public Report Report { get; set; }

        public string ParameterId { get; set; }

        public Parameter Parameter { get; set; }

        public string TypeViewId { get; set; }

        public TypeView TypeView { get; set; }
     
        public JsonDocument Args { get; set; }
    }
}
