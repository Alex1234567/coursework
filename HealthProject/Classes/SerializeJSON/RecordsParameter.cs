﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HealthProject.Classes.SerializeJSON
{
    public class RecordsUnitParameter
    {
        public string UnitParameterId { get; set; }

        public List<DateValue> DatesValues { get; set; }
    }
}
