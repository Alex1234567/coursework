﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace HealthProject.Classes.SerializeJSON
{
    public class DateValue
    {
        public DateTime Date { get; set; }
        public double Value { get; set; }
    }
}
