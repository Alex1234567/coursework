﻿using HealthProject.Models;
using MetadataExtractor;
using MetadataExtractor.Formats.Exif;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace HealthProject.Classes.Import
{
    public class ImportMethods
    {
        public static TonometerRecord RecognizeTonometerPhoto(string pathToImportImages, IFormFile uploadedFile, string pathToPythonScript, string pathToPythonInterpreter, bool isPhotoFromMobile)
        {
            //распознаем
            var myUniqueFileName = string.Format(@"{0}.jpg", Guid.NewGuid());
            var pathToImage = Path.Combine(pathToImportImages, myUniqueFileName);

            // save file
            using (var fileStream = new FileStream(pathToImage, FileMode.Create))
            {
                uploadedFile.CopyTo(fileStream);
            }
            var fullPathToImage = Path.GetFullPath(pathToImage);
            var res =
                Classes.Import.ImportMethods.RunFromCmd(pathToPythonScript, fullPathToImage, pathToPythonInterpreter);
            //D:/recognize_digits/venv/Include/tonometer2/4.jpg

            var values = res.Replace("\r", "").Replace(" ", "").Split('\n');

            int tp, lp, p;
            int.TryParse(values[0], out tp);
            int.TryParse(values[1], out lp);
            int.TryParse(values[2], out p);

            return new TonometerRecord
            {
                TopPressure = tp,
                LowerPressure = lp,
                Pulse = p,
                Date = DatePhoto(pathToImage, isPhotoFromMobile),//DateTime.Now,//
                PathFile = pathToImage
            };
        }

        public static string RunFromCmd(string scriptPath, string pathPhoto, string pathToPythonInterpreter)
        {
            string file = scriptPath;
            string result = "";

            try
            {
                var info = new ProcessStartInfo(pathToPythonInterpreter);
                info.Arguments = (scriptPath + " " + pathPhoto);

                info.RedirectStandardInput = true;
                info.RedirectStandardOutput = true;
                info.UseShellExecute = false;
                info.CreateNoWindow = true;

                using (var proc = new Process())
                {
                    proc.StartInfo = info;
                    proc.Start();
                    proc.WaitForExit();
                    if (proc.ExitCode == 0)
                    {
                        result = proc.StandardOutput.ReadToEnd();
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception("R Script failed: " + result, ex);
            }
        }

        public static DateTime DatePhoto(string path, bool isPhotoFromMobile)
        {

            IEnumerable<MetadataExtractor.Directory> directories = ImageMetadataReader.ReadMetadata(path);
            var subIfdDirectory = directories.OfType<ExifSubIfdDirectory>().FirstOrDefault();
            var stringDateTime = (subIfdDirectory?.GetDescription(ExifDirectoryBase.TagDateTimeOriginal));

            DateTime dateTime = DateTime.Now;

            if (isPhotoFromMobile && stringDateTime == null)
                dateTime = DateTime.MinValue;
            else
            if(stringDateTime != null)
                DateTime.TryParseExact(stringDateTime, "yyyy:MM:dd HH:mm:ss", CultureInfo.InvariantCulture,
                       DateTimeStyles.None, out dateTime);

            //foreach (var directory in directories)
            //    foreach (var tag in directory.Tags)
            //        Console.WriteLine($"{directory.Name} - {tag.Name} = {tag.Description}");

            //var date = System.IO.File.GetLastWriteTime("example.jpg");
            //TimeSpan ts = (System.IO.File.GetLastWriteTime("example.jpg") - System.IO.File.GetLastWriteTimeUtc("example.jpg"));
            //DateTime d = new DateTim(date, ts);

            return dateTime;
        }

       
    }
}
