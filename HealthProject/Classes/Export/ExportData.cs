﻿using System;
using CsvHelper;
using System.Collections.Generic;
using System.IO;
using HealthProject.Models;
using System.Globalization;
using HealthProject.Classes.Export;

namespace HealthProject.Classes.ExportData
{
    
    public class RecordForCsv
    {
        public string Parameter { get; set; }
        public string Unit { get; set; }
        public DateTime Date { get; set; }
        public double Value { get; set; }
    }

    public class ExportData
    {
        public static string pathToExportFiles;
        public static string ToFile(IEnumerable<Record> records)
        {
            List<RecordForCsv> recordsCsv = new List<RecordForCsv>();
            foreach(var r in records)
            {
                recordsCsv.Add(new RecordForCsv
                {
                    Parameter = r.UserParameter.UnitParameter.Parameter.Name,
                    Unit = r.UserParameter.UnitParameter.Unit.Name,
                    Date = r.Date,
                    Value = r.Value
                });
            }
            var myUniqueFileName = string.Format(@"{0}.csv", Guid.NewGuid());
            var pathCsvFile = Path.Combine(pathToExportFiles, myUniqueFileName);

            using (var streamWriter = new StreamWriter(pathCsvFile))
            {
                using (var csvWriter = new CsvWriter(streamWriter, CultureInfo.InvariantCulture))
                {
                    // указываем разделитель, который будет использоваться в файле
                    csvWriter.Configuration.Delimiter = ",";
                    // записываем данные в csv файл
                    csvWriter.WriteRecords(recordsCsv);
                }
            }
            return pathCsvFile;
        }

        public static void CsvToEmails(IEnumerable<Record> records, string emails)
        {
            var pathCsvFile = ToFile(records);

            SenderEmails.SendEmailAsync(emails, "Экспорт", "Ваши данные", pathCsvFile);
        }
        
    }
}
