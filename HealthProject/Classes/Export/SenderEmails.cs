﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MimeKit;
using MailKit.Net.Smtp;

namespace HealthProject.Classes.Export
{
    public class SenderEmails
    {
        public static string name, address, password, hostMailServer;
        public static int portMailServer;

        public static async void SendEmailAsync(string emails, string subject, string message, string pathToFile)
        {
            var emailMessage = new MimeMessage();

            emailMessage.From.Add(new MailboxAddress(name, address));

            //добавляем всех адресатов
            var arr_emails = emails.Split(',');
            foreach (var email in arr_emails)
                emailMessage.To.Add(new MailboxAddress("", email.Trim()));

            emailMessage.Subject = subject;

            var builder = new BodyBuilder();

            builder.TextBody = message;

            builder.Attachments.Add(pathToFile);

            emailMessage.Body = builder.ToMessageBody();

            //emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
            //{
            //    Text = message
            //};

            using (var client = new SmtpClient())
            {
                await client.ConnectAsync(hostMailServer, portMailServer, false);
                //client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                await client.AuthenticateAsync(address, password);
                await client.SendAsync(emailMessage);
                await client.DisconnectAsync(true);
            }

        }
    }
}
