﻿using HealthProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HealthProject.Classes.Export
{
    public class ReportParameterWithRecords
    {
        public ReportParameter ReportParameter { get; set; }

        public IEnumerable<Record> Records { get; set; }   
    }
}
