﻿using HealthProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using A = DocumentFormat.OpenXml.Drawing;
using DW = DocumentFormat.OpenXml.Drawing.Wordprocessing;
using PIC = DocumentFormat.OpenXml.Drawing.Pictures;
using System.IO;
using System.Text.Json;

namespace HealthProject.Classes.Export
{
    public class ExportReport
    {
        public static string pathToExportReports;
        public static string CreatePlotImage(IEnumerable<Record> records, JsonDocument args)
        {
            //var rec = records.ToArray();
            //DateTime start = records.

            double[] dataY = (from r in records
                    select r.Value).ToArray();
            double[] dataX = (from r in records
                     select r.Date.ToOADate()).ToArray();

            var plt = new ScottPlot.Plot(600, 300);


            // turn off features which typically shorten tick label size
            plt.Ticks(useOffsetNotation: false, useMultiplierNotation: false);

            // tightening with a render is the best way to get the axes right
            plt.TightenLayout(render: true);

            if(args.RootElement.GetProperty("plot_type").GetString() == "scatter")              
                plt.PlotScatter(dataX, dataY, markerShape: ScottPlot.MarkerShape.none);
            if (args.RootElement.GetProperty("plot_type").GetString() == "bar")
                plt.PlotBar(dataX, dataY);

            plt.Ticks(dateTimeX: true);
            //plt.YLabel("Price");
            //plt.XLabel("Date and Time");
            plt.Title(records.First().UserParameter.UnitParameter.ParameterNameUnitName);

            var myUniqueFileName = string.Format(@"{0}.png", Guid.NewGuid());

            string pathImage = Path.Combine(pathToExportReports, myUniqueFileName);
            plt.SaveFig(pathImage);

            return pathImage;
        }

        public static string CreateWord(List<ReportParameterWithRecords> reportParametersWithRecords, DateTime datetimeBegin, DateTime datetimeEnd)
        {
            var myUniqueFileName = string.Format(@"{0}.docx", Guid.NewGuid());
            string reportPath = Path.Combine(pathToExportReports, myUniqueFileName);


            using (WordprocessingDocument wordDocument =
            WordprocessingDocument.Create(reportPath, WordprocessingDocumentType.Document))
            {
                // Add a main document part. 
                MainDocumentPart mainPart = wordDocument.AddMainDocumentPart();
                
                // Create the document structure and add some text.
                mainPart.Document = new Document();
                Body body = mainPart.Document.AppendChild(new Body());
                Paragraph para = body.AppendChild(new Paragraph());
                Run run = para.AppendChild(new Run());
                run.AppendChild(new Text(
                    string.Format("Отчет для \"{0}\" за период времени с {1} по {2}",
                    reportParametersWithRecords.First().ReportParameter.Report.Name,
                    datetimeBegin.ToString("d"),
                    datetimeEnd.ToString("d"))
                    ));

                foreach(var reportParameterWithRecords in reportParametersWithRecords)
                {
                    if (reportParameterWithRecords.Records.Count() > 0)
                    {
                        if(reportParameterWithRecords.ReportParameter.TypeView.Name == "График")
                        {
                            ImagePart imagePart = mainPart.AddImagePart(ImagePartType.Png);
                            using (FileStream stream = new FileStream(
                                CreatePlotImage(reportParameterWithRecords.Records,
                                reportParameterWithRecords.ReportParameter.Args),
                                FileMode.Open))
                            {
                                imagePart.FeedData(stream);
                            }
                            AddImageToBody(wordDocument, mainPart.GetIdOfPart(imagePart));
                        }
                        if (reportParameterWithRecords.ReportParameter.TypeView.Name == "Таблица")
                        {
                            AddTableToBody(wordDocument, reportParameterWithRecords);                  
                        }
                    }
                }
                
            }
            return reportPath;
        }

        public static void AddTableToBody(WordprocessingDocument wordDoc, ReportParameterWithRecords reportParameterWithRecords)
        {
            // Use the file name and path passed in as an argument 
            // to open an existing Word 2007 document.  
            
            // Create an empty table.
            Table table = new Table();

            // Create a TableProperties object and specify its border information.
            TableProperties tblProp = new TableProperties(
                new TableBorders(
                    new TopBorder()
                    {
                        Val =
                        new EnumValue<BorderValues>(BorderValues.Moons),
                        Size = 3
                    },
                    new BottomBorder()
                    {
                        Val =
                        new EnumValue<BorderValues>(BorderValues.Moons),
                        Size = 3
                    },
                    new LeftBorder()
                    {
                        Val =
                        new EnumValue<BorderValues>(BorderValues.Moons),
                        Size = 3
                    },
                    new RightBorder()
                    {
                        Val =
                        new EnumValue<BorderValues>(BorderValues.Moons),
                        Size = 3
                    },
                    new InsideHorizontalBorder()
                    {
                        Val =
                        new EnumValue<BorderValues>(BorderValues.Moons),
                        Size = 3
                    },
                    new InsideVerticalBorder()
                    {
                        Val =
                        new EnumValue<BorderValues>(BorderValues.Moons),
                        Size = 3
                    }
                )
            );

            // Append the TableProperties object to the empty table.
            table.AppendChild<TableProperties>(tblProp);

            // Create table name.
            TableRow trTableName = new TableRow();
            TableCell tcTableName = new TableCell();
            TableCell tcMerged = new TableCell();
            tcTableName.TableCellProperties = new TableCellProperties();
            tcTableName.TableCellProperties.HorizontalMerge = new HorizontalMerge { Val = MergedCellValues.Restart };
            tcMerged.TableCellProperties = new TableCellProperties();
            tcMerged.TableCellProperties.HorizontalMerge = new HorizontalMerge { Val = MergedCellValues.Continue };

            //center aligment for tableName
            Paragraph paraTableName = new Paragraph();
            ParagraphProperties paraProperties = new ParagraphProperties();
            Justification justification = new Justification() { Val = JustificationValues.Center };
            paraProperties.Append(justification);
            paraTableName.Append(paraProperties);
            paraTableName.Append(new Run(new Text(reportParameterWithRecords.Records.First().UserParameter.UnitParameter.Parameter.Name)));

            Paragraph para1 = new Paragraph(new Run(new Text("")));
            ParagraphProperties paraProperties1 = new ParagraphProperties();
            Justification justification1 = new Justification() { Val = JustificationValues.Center };
            paraProperties.Append(justification1);
            para1.Append(paraProperties1);

            tcTableName.Append(paraTableName);
            tcMerged.Append(para1);

            trTableName.Append(tcTableName);
            trTableName.Append(tcMerged);
            

            table.Append(trTableName);

            // Create headers.
            TableRow trHeaders = new TableRow();
            TableCell tcValueHeader = new TableCell();
            TableCell tcVDateHeader = new TableCell();
            tcValueHeader.Append(new Paragraph(new Run(new Text(string.Format("Значение, {0}", reportParameterWithRecords.Records.First().UserParameter.UnitParameter.Unit.Name)))));
            tcVDateHeader.Append(new Paragraph(new Run(new Text("Дата"))));
            trHeaders.Append(tcValueHeader);
            trHeaders.Append(tcVDateHeader);
            table.Append(trHeaders);

            foreach (var record in reportParameterWithRecords.Records)
            {
                // Create a row.
                TableRow tr = new TableRow();

                // Create a cell.
                TableCell tcValue = new TableCell();

                TableCell tcDate = new TableCell();

                //// Specify the width property of the table cell.
                //tc1.Append(new TableCellProperties(
                //    new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "2400" }));

                // Specify the table cell content.
                tcValue.Append(new Paragraph(new Run(new Text(record.Value.ToString("F")))));
                tcDate.Append(new Paragraph(new Run(new Text(record.Date.ToString("F")))));

                // Append the table cell to the table row.
                tr.Append(tcValue);
                tr.Append(tcDate);

                // Append the table row to the table.
                table.Append(tr);
            }
            // Append the table to the document.
            wordDoc.MainDocumentPart.Document.Body.Append(table);
        }

        private static void AddImageToBody(WordprocessingDocument wordDoc, string relationshipId)
        {
            // Define the reference of the image.
            var element =
                 new Drawing(
                     new DW.Inline(
                         new DW.Extent() { Cx = 600 * 9525, Cy = 300 * 9525 },
                         new DW.EffectExtent()
                         {
                             LeftEdge = 0L,
                             TopEdge = 0L,
                             RightEdge = 0L,
                             BottomEdge = 0L
                         },
                         new DW.DocProperties()
                         {
                             Id = (UInt32Value)1U,
                             Name = "Picture 1"
                         },
                         new DW.NonVisualGraphicFrameDrawingProperties(
                             new A.GraphicFrameLocks() { NoChangeAspect = true }),
                         new A.Graphic(
                             new A.GraphicData(
                                 new PIC.Picture(
                                     new PIC.NonVisualPictureProperties(
                                         new PIC.NonVisualDrawingProperties()
                                         {
                                             Id = (UInt32Value)0U,
                                             Name = "New Bitmap Image.png"
                                         },
                                         new PIC.NonVisualPictureDrawingProperties()),
                                     new PIC.BlipFill(
                                         new A.Blip(
                                             new A.BlipExtensionList(
                                                 new A.BlipExtension()
                                                 {
                                                     Uri =
                                                        "{28A0092B-C50C-407E-A947-70E740481C1C}"
                                                 })
                                         )
                                         {
                                             Embed = relationshipId,
                                             CompressionState =
                                             A.BlipCompressionValues.Print
                                         },
                                         new A.Stretch(
                                             new A.FillRectangle())),
                                     new PIC.ShapeProperties(
                                         new A.Transform2D(
                                             new A.Offset() { X = 0L, Y = 0L },
                                             new A.Extents() { Cx = 600 * 9525, Cy = 300 * 9525 }),
                                         new A.PresetGeometry(
                                             new A.AdjustValueList()
                                         )
                                         { Preset = A.ShapeTypeValues.Rectangle }))
                             )
                             { Uri = "http://schemas.openxmlformats.org/drawingml/2006/picture" })
                     )
                     {
                         DistanceFromTop = (UInt32Value)0U,
                         DistanceFromBottom = (UInt32Value)0U,
                         DistanceFromLeft = (UInt32Value)0U,
                         DistanceFromRight = (UInt32Value)0U,
                         EditId = "50D07946"
                     });

            // Append the reference to body, the element should be in a Run.
            wordDoc.MainDocumentPart.Document.Body.AppendChild(new Paragraph(new Run(element)));
        }

       

        public static void ReportToEmails(List<ReportParameterWithRecords> reportParametersWithRecords, DateTime datetimeBegin, DateTime datetimeEnd, string emails)
        {
            var pathReport = CreateWord(reportParametersWithRecords, datetimeBegin, datetimeEnd);

            SenderEmails.SendEmailAsync(emails, "Экспорт", "Ваш отчет", pathReport);
        }
    }
}
