﻿using HealthProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HealthProject.ViewModels
{
    public class RecordsModel
    {
        public IEnumerable<Record> Records;
        public UserParameter UserParameter;
    }
}
