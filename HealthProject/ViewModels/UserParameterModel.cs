﻿using HealthProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HealthProject.ViewModels
{
    public class UserParameterModel
    {
        public UserParameter UserParameter { get; set; }
        public List<SimpleRecordModel> Records { get; set; }
    }
}
