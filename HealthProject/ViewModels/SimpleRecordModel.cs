﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HealthProject.ViewModels
{
    public class SimpleRecordModel
    {
        public DateTime Date { get; set; }

        public double Value { get; set; }
    }
}
