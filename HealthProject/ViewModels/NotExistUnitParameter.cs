﻿namespace HealthProject.ViewModels
{
    public class NotExistUnitParameter
    {
        public string ParameterIdUnitId { get; set; }
        public string ParameterNameUnitName { get; set; }        
    }
}
