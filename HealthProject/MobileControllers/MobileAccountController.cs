﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using HealthProject.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace HealthProject.MobileController
{
    [Route("api/[controller]")]
    [ApiController]
    public class MobileAccountController : ControllerBase
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;

        string jwtConstantIssuer;
        string jwtConstantAudience;
        string jwtConstantKey;
        public MobileAccountController(UserManager<User> userManager, SignInManager<User> signInManager, IConfiguration configuration)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            var jwtConstants = configuration.GetSection("JwtConstants");
            this.jwtConstantIssuer = jwtConstants.GetValue<string>("Issuer");
            this.jwtConstantAudience = jwtConstants.GetValue<string>("Audience");
            this.jwtConstantKey = jwtConstants.GetValue<string>("Key");
        }

        [HttpPost]
        [Route("Register")]
        public async Task<IActionResult> Register(ViewModels.RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                User user = new User
                {
                    Email = model.Email,
                    UserName = model.Email//, Year = model.Year 
                };
                // добавляем пользователя
                var result = await _userManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    // установка куки
                    await _signInManager.SignInAsync(user, false);
                    return Ok();
                }
                else
                {
                    return BadRequest("Пользователь с таким почтовым адресом уже зарегистрирован");
                }
            }
            return BadRequest();
        }
        [HttpPost]
        [Route("CreateToken")]
        public async Task<IActionResult> CreateToken(LoginModel model)
        {
            if(ModelState.IsValid)
            {
                var user = await _userManager.FindByNameAsync(model.UserName);
                if(user != null)
                {
                    var signInResult = await _signInManager.CheckPasswordSignInAsync(user, model.Password, false);
                    if (signInResult.Succeeded)
                    {
                        var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtConstantKey));
                        var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

                        var claims = new[] { new Claim("user_id", user.Id) };

                        var token = new JwtSecurityToken(
                            jwtConstantIssuer,
                            jwtConstantAudience,
                            claims,
                            //expires: DateTime.UtcNow.AddMinutes(60),
                            signingCredentials: creds
                            );

                        var result = new JwtSecurityTokenHandler().WriteToken(token);

                        return Created("", result);
                    }
                    else
                        return BadRequest("Неверный логин или пароль");
                }
                else
                    return BadRequest("Неверный логин или пароль");                   
            }
            return BadRequest();
        }
    }
}






