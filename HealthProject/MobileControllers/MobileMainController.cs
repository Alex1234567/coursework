﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using HealthProject.MobileModels;
using HealthProject.Models;
using HealthProject.ViewModels;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace HealthProject.MobileControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MobileMainController : ControllerBase
    {
        private readonly HealthContext health_db;
        string mobileInterfaceSystemId; 
        string pathToPythonScript;
        string pathToPythonInterpreter;
        string pathToImportImages;
        public MobileMainController(HealthContext health_db, IConfiguration configuration)
        {
            this.health_db = health_db;
            var identificatorsConfig = configuration.GetSection("Identificators");
            this.mobileInterfaceSystemId = identificatorsConfig.GetValue<string>("MobileInterfaceSystemId");

            var pathsConfig = configuration.GetSection("Paths");
            this.pathToPythonScript = pathsConfig.GetValue<string>("PathToPythonScript");
            this.pathToPythonInterpreter = pathsConfig.GetValue<string>("PathToPythonInterpreter");
            this.pathToImportImages = pathsConfig.GetValue<string>("PathToImportImages");
        }


        [HttpGet]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [Route("AddUserParameter")]
        public List<MobileModels.UnitParameter> AddUserParameter()
        {
            var userId = User.FindFirstValue("user_id");
            //var userId = db.Users.FirstOrDefault(user => user.UserName == userName).Id;

            return health_db.ListForManageUserParameters(userId);
        }

        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [Route("AddUserParameter")]
        public ActionResult AddUserParameter([FromBody] object unitParameterId)
        {
            var upId = unitParameterId.ToString();

            var userId = User.FindFirstValue("user_id");

            var userParameterId = health_db.AddUserParameter(userId, upId, true);

            return Ok(userParameterId);
        }

        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [Route("UnsubscribeUserParameter")]
        public ActionResult UnsubscribeUserParameter([FromBody] object userParameterId)
        {
            HealthContext db = health_db;

            var upId = userParameterId.ToString();

            health_db.UnsubscribeUserParameter(upId);

            return Ok();
        }

        [HttpGet]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [Route("MainUserParameters")]
        public List<MobileModels.UserParameter> MainUserparameters()
        {
            var userId = User.FindFirstValue("user_id");

            return health_db.UserParameters(userId, true);
        }

        [HttpGet]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [Route("SecondUserParameters")]
        public List<MobileModels.UserParameter> SecondUserParameters()
        {
            var userId = User.FindFirstValue("user_id");

            return health_db.UserParameters(userId, false);
        }

        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [Route("GetRecords")]
        public List<MobileModels.Record> GetRecords([FromBody] object userParameterId)
        {
            var upId = userParameterId.ToString();

            HealthContext db = health_db;

            var records = from record in db.Records
                          where record.UserParameterId == upId
                          orderby record.Date
                          select new MobileModels.Record
                          {
                              Id = record.Id,
                              Date = record.Date,
                              Value = record.Value
                          };

            return records.ToList();
        }

        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [Route("AddRecord")]
        public ActionResult AddRecord(MobileModels.NewRecord newRecMobileModel)
        {
            HealthContext db = health_db;

            var userId = User.FindFirstValue("user_id");

            HealthProject.Models.Record record = new Models.Record
            {
                UserParameterId = newRecMobileModel.UserParameterId,
                SystemId = mobileInterfaceSystemId,
                Date = newRecMobileModel.Date,
                Value = newRecMobileModel.Value
            };

            db.Records.Add(record);

            db.SaveChanges();

            return Ok(record.Id);
        }

        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [Route("DeleteRecord")]
        public ActionResult DeleteRecord([FromBody] object recordId)
        {
            var recdId = recordId.ToString();

            HealthContext db = health_db;

            var record = health_db.Records.Find(recdId);

            health_db.Remove(record);

            health_db.SaveChanges();

            return Ok();
        }

        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [Route("UnitParametersForParameter")]
        public List<MobileModels.UnitParameter> UnitParametersForParameter([FromBody] object userParameterId)
        {
            var userPId = userParameterId.ToString();

            var userParameter = health_db.UsersParameters.Find(userPId);

            var unitParameter = health_db.UnitsParameters.Find(userParameter.UnitParameterId);

            //берем показатели с единицами измерения у которых нужный показатель 
            var upForParameter = from up in health_db.UnitsParameters.Include(up => up.Unit).Include(up => up.Parameter)
                                 where up.ParameterId == unitParameter.ParameterId
                                 && up.Id != unitParameter.Id//исключаем показатель с исходными единицами измерения (нет смысла менять единицы измерения на те же)
                                 orderby up.Parameter.Name
                                 select new MobileModels.UnitParameter
                                 {
                                     Id = up.Id,
                                     ParameterName = up.Parameter.Name,
                                     UnitName = up.Unit.Name,
                                     UserParameterId = userPId
                                 };

            return upForParameter.ToList();
        }

        public void Convert(double coefficient, string userParameterId, string newUnitParameter)
        {
            var userParameter = health_db.UsersParameters.Find(userParameterId);

            userParameter.UnitParameterId = newUnitParameter;

            var records = from r in health_db.Records
                          where r.UserParameterId == userParameterId
                          select r;

            foreach (var r in records)
                r.Value *= coefficient;

            health_db.SaveChanges();
        }

        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [Route("ChangeUnitParameter")]
        public ActionResult ChangeUnitParameter(ChangeUnitParameter changeUnitParameter)
        {
            bool isTargetUnitBase, isCurrentUnitBase;

            //находим показатель с ед. изм. по id в который пользователь хочет конвертировать текущий
            var targetUnitParameter = health_db.UnitsParameters.Find(changeUnitParameter.TargetUnitParameterId);

            //находим подписку на текущий показатель с ед. изм.
            var userParameter = health_db.UsersParameters.Find(changeUnitParameter.UserParameterId);

            //находим текущий показатель с ед. изм. пользователя
            var currentUnitParameter = health_db.UnitsParameters.Find(userParameter.UnitParameterId);

            //если нашлась строчка в которой базовый id является id целевых ед. изм., то  целевые ед. изм. базовые
            isTargetUnitBase = health_db.TranslatorsUnits.FirstOrDefault(tu => tu.BaseUnitId == targetUnitParameter.UnitId) != null ? true : false;

            //если нашлась строчка в которой базовый id является id текущих ед. изм., то  текущие ед. изм. базовые
            isCurrentUnitBase = health_db.TranslatorsUnits.FirstOrDefault(tu => tu.BaseUnitId == currentUnitParameter.UnitId) != null ? true : false;

            double coefficientForConvert = 1;

            if (!isCurrentUnitBase && !isTargetUnitBase)
            {
                //находим коэффициент для конвертации из текущих в базовые ед.изм
                double c1 = health_db.TranslatorsUnits.FirstOrDefault(tu => tu.UnitId == currentUnitParameter.UnitId).Coefficient;
                //находим коэффициент для конвертации из базовых в целевые ед.изм
                double c2 = health_db.TranslatorsUnits.FirstOrDefault(tu => tu.UnitId == targetUnitParameter.UnitId).Coefficient;

                coefficientForConvert *= (c1 / c2);
            }
            else
            if (isCurrentUnitBase)
                coefficientForConvert /= health_db.TranslatorsUnits.FirstOrDefault(
                    tu => tu.BaseUnitId == currentUnitParameter.UnitId &&
                    tu.UnitId == targetUnitParameter.UnitId).Coefficient;
            else
                coefficientForConvert *= health_db.TranslatorsUnits.FirstOrDefault(
                    tu => tu.BaseUnitId == targetUnitParameter.UnitId &&
                    tu.UnitId == currentUnitParameter.UnitId ).Coefficient;

            Convert(coefficientForConvert, changeUnitParameter.UserParameterId, targetUnitParameter.Id);

            return Ok();
        }

        [HttpGet]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [Route("AllParametersAndUnits")]
        public ActionResult AllParametersAndUnits()
        {
            var userId = User.FindFirstValue("user_id");

            var parametersNames = (from up in health_db.UnitsParameters
                                  where (up.AuthorUserId == null ||
                                  up.AuthorUserId == userId)
                                  select up.Parameter.Name.ToLower()).Distinct();

            return Ok(
            new MobileModels.ParametersAndUnits
            {
                ParametersNames = parametersNames.ToList(),
                Units = health_db.AvailableUnitsForUser(userId)
            });
        }

        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [Route("AddUnitParameter")]
        public ActionResult AddUnitParameter(MobileModels.NewUnitParameter newUnitParameter)
        {
            var userId = User.FindFirstValue("user_id");

            string errors = "";
            if (!health_db.IsUniqParameterNameForUser(userId, newUnitParameter.ParameterName))
                errors = "Похожий показатель уже существует\n";
            if(newUnitParameter.UnitId == null && !health_db.IsUniqUnitNameForUser(userId, newUnitParameter.UnitName))
                errors += "Похожие ед. изм. уже существуют";

            if(errors != "")
                return BadRequest(errors);

            health_db.AddUnitParameter(userId,newUnitParameter.ParameterName, 
                newUnitParameter.UnitId, newUnitParameter.UnitName);

            return Ok();
        }


        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [Route("ChangeRecord")]
        public ActionResult ChangeRecord(MobileModels.Record mobileRecord)
        {

            var record = health_db.Records.Find(mobileRecord.Id);

            record.Value = mobileRecord.Value;
            record.Date = mobileRecord.Date;

            health_db.SaveChanges();

            return Ok();
        }


        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [Route("RecognizePhotoTonometer")]
        public TonometerRecord RecognizePhotoTonometer(IFormFile uploadedFile)
        {
            return Classes.Import.ImportMethods.RecognizeTonometerPhoto
                (pathToImportImages, uploadedFile, pathToPythonScript, pathToPythonInterpreter, true);
        }

        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [Route("ConfirmRecognizedValues")]
        public ActionResult ConfirmRecognizedValues(TonometerRecord tonometerRecord)
        {
            var userId = User.FindFirstValue("user_id");

            health_db.ConfirmRecognizedValues(userId, tonometerRecord);
            return Ok();
        }

        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [Route("ChangeUserParameterSection")]
        public ActionResult ChangeUserParameterSection([FromBody] object userParameterId)
        {
            health_db.ChangeUserParameterSection(userParameterId.ToString());

            return Ok();
        }
    }
}
